from django.shortcuts import render
from django.http import HttpResponse

from .models import Greeting

# Create your views here.
def index(request):
    # return HttpResponse('Hello from Python!')
    return render(request, "index.html")


def db(request):
    try:
        print("Start db call")
        greeting = Greeting()
        print("save db")
        greeting.save()
        print("get db")
        greetings = Greeting.objects.all()
        #greetings=[]
    except e:
        print(e)
    print("Some log")
    return render(request, "db.html", {"greetings": greetings})
